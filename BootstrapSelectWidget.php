<?php

namespace azbuco\bootstrapselect;

use yii\base\InvalidConfigException;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\InputWidget;

class BootstrapSelectWidget extends InputWidget
{
    /**
     * Items for dropDownList
     * @var array
     */
    public $items = [];

    /**
     * Plugin options
     * @see https://silviomoreto.github.io/bootstrap-select/ 
     * @var array
     */
    public $clientOptions = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        
        $this->options['data-bootstrapselect-id'] = $this->getId();
        
        $this->registerBundle();
        $this->registerClientScript();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        parent::run();
        $this->registerJs();
        return $this->hasModel() ?
            Html::activeDropDownList($this->model, $this->attribute, $this->items, $this->options) :
            Html::dropDownList($this->name, $this->value, $this->items, $this->options);
    }

    protected function registerBundle()
    {
        BootstrapSelectAsset::register($this->getView());
    }

    private function registerClientScript()
    {
        $view = $this->getView();
        $options = Json::encode($this->clientOptions);
        Html::addCssClass($this->options, $this->cssClass);
        Html::addCssClass($this->options, 'form-control');

        $view->registerJs(';jQuery("[data-bootstrapselect-id=\'' . $this->getId() . '\']").selectpicker(' . $options . ');"');
    }
}
