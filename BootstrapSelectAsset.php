<?php

namespace azbuco\bootstrapselect;

use yii\web\AssetBundle;

class BootstrapSelectAsset extends AssetBundle
{
    public $sourcePath = '@bower/bootstrap-select';
    public $js = [
        'dist/js/bootstrap-select.min.js',
    ];
    public $css = [
        'dist/css/bootstrap-select.min.css',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}
